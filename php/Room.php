<?php
	class Room {
		
		private $id;
		private $name;
		private $hp;
		private $level;
		private $maxUsers;
		private $type;
		private $currentLastAction;
		private $currentHp;
		private $winCount;
		private $lossCount;
		private $lastTarget;
		private $nb;
		private $exp;

		public function __construct($id, $name,$hp, $level, $maxUsers, $type, $currentLastAction, $currentHp, $winCount, $lossCount, $lastTarget, $nb, $exp) {
			$this->id = $id;
			$this->name = $name;
			$this->hp = $hp;
			$this->level = $level;
			$this->maxUsers = $maxUsers;
			$this->type = $type;
			$this->currentLastAction = $currentLastAction;
			$this->currentHp = $currentHp;
			$this->winCount = $winCount;
			$this->lossCount = $lossCount;
			$this->lastTarget = $lastTarget;
			$this->nb = $nb;
			$this->exp = $exp;
		}
		
		public function id() {
			return $this->id;
		}
		public function name() {
			return $this->name;
		}
		public function level() {
			return $this->level;
		}
		public function maxUsers() {
			return $this->maxUsers;
		}
		public function type() {
			return $this->type;
		}
		public function currentLastAction() {
			return $this->currentLastAction;
		}
		public function currentHp() {
			return $this->currentHp;
		}
		public function winCount() {
			return $this->winCount;
		}
		public function lossCount() {
			return $this->lossCount;
		}
		public function lastTarget() {
			return $this->lastTarget;
		}
		public function nb() {
			return $this->nb;
		}
		public function exp() {
			return $this->exp;
		}
		public function hp() {
			return $this->hp;
		}
	}
?>