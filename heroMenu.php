<!DOCTYPE html>
<html>
<head>
	<title>Little Star Wars</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/heroMenu.css">
	<script type="text/javascript" src="js/TiledImage.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/heroMenu.js"></script>
</head>
<body>
	<div id="hero"></div>

	<div id="loadingScreen">
		<div id="loadingAnimation"></div>
	</div>
	<div id="heroStatsContainer">
		<div id="heroStats">
			<script type="x-script" id="template">
				<p class='statName'></p>
				<p class='statValue'></p>
			</script>
		</div>
	</div>

	<div>
		<form action="gameMenu.php" method="post" id="comeBack" onclick="goToGameMenu(this)">
			<button type="submit" id="comeBackButton"></button>
		</form>
	</div>
</div>
</body>
</html>