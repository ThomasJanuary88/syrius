<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Little Star Wars</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<script type="text/javascript" src="js/TiledImage.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
	<audio hidden controls autoplay id="audio1">
  		<source src="assets/musics/index/background.mp3" type="audio/mpeg">
	</audio>
	<audio hidden controls autoplay id="audio2">
  		<source src="assets/musics/index/crow_call.mp3" type="audio/mpeg" >
	</audio>
</head>
<body>
	<div id="login">
		<form action="index.php" method="post">
			<p>Nom du compte</p>
			<div class="text">
				<input type="text" name="nom_compte" required></input>
			</div>
			<p>Mot de passe</p>
			<div>
				<input type="password" name="mdp" required>
			</div>
			<div>
				<button type="submit">Login</button>
			</div>
		</form>
	</div>
	<div id="message_erreur">
		<?php
			if (isset($_SESSION["error"])) {
				echo($_SESSION["error"]);
			}
		?>
	</div>
</body>
</html>