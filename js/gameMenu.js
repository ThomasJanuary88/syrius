let opacityInterval = null;
let listRoom = null;
let premierAppel = null;

window.onload = function() {

	let audio = document.getElementById("audio1");
	audio.pause();

	//créer les nodes de séléction de mission + les menus
	for (let i = 0; i < 5; i++) {
		createVerticalMenu(i);
		createNodeMission(i);
	}

	premierAppel = true;
	setInterval(() => {
		call_api_info_menu();
	}, 2900);

	//Loadingscreen
	loadingScreen();
}

function loadingScreen() {
	let soliderAnimated = new SoldierSprite();
	let nbNodes = document.body.childElementCount +6;
	for (let i = 0; i < nbNodes; i++){
		let node = document.body.childNodes[i];
		if (node.style != undefined) {
			node.style.visibility = 'hidden';
		}
		
	}
	let loadingScreen = document.getElementById('loadingScreen');
	loadingScreen.style.visibility = 'visible';

	setTimeout(() => {
		for (let i = 0; i < nbNodes; i++){
			let node = document.body.childNodes[i];
			if (node.style != undefined && node.className != "vertical_menu") {
				node.style.visibility = 'visible';
			}
			
		}
		let loadingScreen = document.getElementById('loadingScreen');
		loadingScreen.style.visibility = 'hidden';

		//Ajuster le volume des musics de fond
		let audio = document.getElementById("audio1");
		audio.play();
		audio.volume=0.3;
		audio.loop=true;
	},3100);
}

class SoldierSprite {
	constructor() {
		this.node = document.getElementById('loadingAnimation');
		this.x = this.node.offsetLeft;
		this.y = this.node.offsetTop;
		this.sprite = new TiledImage("assets/images/items/loading_screen.png", 30, 1, 10, true, 1.0, this.node.id);
		this.sprite.changeMinMaxInterval(1, 29);
		this.tick();
	}
	tick() {
		setInterval(() => {
			this.sprite.tick(this.x, this.y);
		},40);
	}
}

function setupParchment() {
	for (let i = 0; i < listRoom.length; i++) {
		let room = listRoom[i];

		if (room.level <= 4) {
			createParchments(0, room.name ,room.level, room.max_users, room.hp, room.type, room.id, room.nb);
		}
		else if (room.level <=10 ) {
			createParchments(1, room.name ,room.level, room.max_users, room.hp, room.type, room.id, room.nb);
		}
		else if (room.level <= 15) {
			createParchments(2, room.name ,room.level, room.max_users, room.hp, room.type, room.id, room.nb);
		}
		else if (room.level <= 20) {
			createParchments(3, room.name ,room.level, room.max_users, room.hp, room.type, room.id, room.nb);
		}
		else {
			createParchments(4, room.name ,room.level, room.max_users, room.hp, room.type, room.id, room.nb);
		}
	}
}

function setupNodeMission() {
	lastRoom = listRoom.slice(-1)[0];
	lastRoomLevel = lastRoom.level;
	updateNodeMissionColor(lastRoomLevel);
}

function updateParchment() {
	let k = 0;
	for (let i =0; i < 5; i++) {
		let container = document.getElementById('vertical_menu_'+i);
		let parchmentChilds = container.childNodes;

		for (let j = 0; j < parchmentChilds.length; j ++) {
			let nodeText = parchmentChilds[j].childNodes[2].childNodes[2];
			let room = listRoom[k];
			let nb = room.nb;
			let mx = room.max_users;
			let txt = "<b>Co-op :</b>"+nb+"/"+mx;
			nodeText.innerHTML = txt;
			k += 1;
		}
	}
}

function call_api_info_menu() {
	$.ajax({
			type : "POST",
			url : "ajaxMenu.php"
		})
		.done(response => {
			console.log(response);
			data = JSON.parse(response);
			listRoom = data;

			if (premierAppel) {
				setupParchment();
				setupNodeMission();

			}
			else {
				updateParchment();
			}
			premierAppel = false;
		});

}

function createNodeMission(id) {
	gameMenuScreen = document.getElementById("body");
	nodeMission = document.createElement("div");
	nodeMission.setAttribute("class", "mission_circle");
	nodeMission.setAttribute("id", "mission_"+id);
	nodeMission.setAttribute("onclick", "showMission(this)");
	nodeMission.onmouseover = function () {
		giveFeedback(this);
	};
	nodeMission.onmouseout = function () {
		let nodeText = document.getElementById("textFeedback");
		lowerOpacity(nodeText);
	};
	gameMenuScreen.appendChild(nodeMission);
}

function giveFeedback(node) {
	clearInterval(opacityInterval);
	let nodeText = document.getElementById("textFeedback");
	nodeText.style.opacity = 1;
	if (getComputedStyle(node, null).getPropertyValue("background-color") == "rgb(128, 128, 128)") {
		nodeText.innerHTML = "Your level is to low.";
	}
	else if (node.id == "brouette") {
		nodeText.innerHTML = "Hero stats.";
	}
	else {
		nodeText.innerHTML = "";
	}
}

function lowerOpacity(opacity) {
	opacityInterval = setInterval(() => {
		let nodeText = document.getElementById("textFeedback");
		let currentOpacity = getComputedStyle(nodeText, null).getPropertyValue('opacity');
		if (currentOpacity > 0.0) {
			currentOpacity -= 0.05;
			nodeText.style.opacity = currentOpacity;
		}
		else {
			clearInterval(opacityInterval);
		}
	}, 100);

}

function createVerticalMenu(id) {
	gameMenuScreen = document.getElementById("body");
	verticalMission = document.createElement("div");
	verticalMission.setAttribute("class", "vertical_menu");
	verticalMission.setAttribute("id", "vertical_menu_"+id);
	gameMenuScreen.appendChild(verticalMission);
}

function updateNodeMissionColor(firstLevelRoom) {
	if (firstLevelRoom <= 4) {
		document.getElementById("mission_1").setAttribute("style", "background-color: grey;");
		document.getElementById("mission_2").setAttribute("style", "background-color: grey;");
		document.getElementById("mission_3").setAttribute("style", "background-color: grey;");
		document.getElementById("mission_4").setAttribute("style", "background-color: grey;");
	}
	else if (firstLevelRoom <= 10) {
		document.getElementById("mission_2").setAttribute("style", "background-color: grey;");
		document.getElementById("mission_3").setAttribute("style", "background-color: grey;");
		document.getElementById("mission_4").setAttribute("style", "background-color: grey;");

	}
	else if (firstLevelRoom <= 15) {
		document.getElementById("mission_3").setAttribute("style", "background-color: grey;");
		document.getElementById("mission_4").setAttribute("style", "background-color: grey;");

	}
	else if (firstLevelRoom <= 20) {
		document.getElementById("mission_4").setAttribute("style", "background-color: grey;");

	}
}

//céer un parchemin pour une room
function createParchments(idVerticalMenu, roomName, roomLevel, roomMaxUsers, roomHp, roomType, roomId, nbUsers) {
	let verticalMenu = document.getElementById("vertical_menu_"+idVerticalMenu);
	
	let parchment = document.createElement("form");
	parchment.setAttribute("method", "get");

	let parchmentInput1 = document.createElement("input");
	parchmentInput1.setAttribute("name", "roomId");
	parchmentInput1.setAttribute("value", roomId);
	let parchmentInput2 = document.createElement("input");
	parchmentInput2.setAttribute("name", "missionLevel");


	
	let parchmentContent = document.createElement("div");
	parchmentContent.setAttribute("class", "room");
	parchmentContent.setAttribute("onclick", "javascript:this.parentNode.submit()");


	let name = document.createElement("p");
	name.innerHTML = "<b>Name :</b>"+roomName;
	let level = document.createElement("p");
	level.innerHTML = "<b>Level :</b>"+roomLevel;
	let maxUsers = document.createElement("p");
	maxUsers.innerHTML = "<b>Co-op :</b>"+nbUsers+"/"+roomMaxUsers;
	let hp = document.createElement("p");
	hp.innerHTML = "<b>Hp :</b>"+roomHp;
	let type = document.createElement("p");
	type.innerHTML = "<b>Type :</b>"+roomType;

	parchmentContent.appendChild(name);
	parchmentContent.appendChild(level);
	parchmentContent.appendChild(maxUsers);
	parchmentContent.appendChild(hp);
	parchmentContent.appendChild(type);


	parchment.appendChild(parchmentInput1);
	parchment.appendChild(parchmentInput2);
	parchment.appendChild(parchmentContent);
	verticalMenu.appendChild(parchment);

	let missionLevel = parchmentInput2.parentNode.parentNode.id.substr(parchmentInput2.parentNode.parentNode.id.length - 1);
	parchmentInput2.setAttribute("value", missionLevel);
}

function showMission(element) {
	play_sound("assets/sounds/clic.mp3", 1);
	let id = element.id.substr(element.id.length - 1);
	let allVerticalMenu = document.getElementsByClassName("vertical_menu");

	if (getComputedStyle(element, null).getPropertyValue("background-color") == "rgb(128, 128, 128)") {
		Array.from(allVerticalMenu).forEach(function (verticalMenu) {
			verticalMenu.style.visibility = "hidden";
		});

	}
	else {

		Array.from(allVerticalMenu).forEach(function (verticalMenu) {
			verticalMenu.style.visibility = "hidden";
		});

		if (document.getElementById("vertical_menu_"+id).style.visibility == 'hidden') {
			play_sound("assets/sounds/item/openParchment.mp3", 1);
		}
		document.getElementById("vertical_menu_"+id).style.visibility = "visible";
	}
}


function play_sound(path, volume=0.1) {
	let a = new Audio(path);
	a.volume = volume;
	a.play();
}

function goToHeroMenu(element) {
	play_sound('assets/sounds/clic.mp3', 1);

	setTimeout(() => {
		element.submit();
	}, 250);

}