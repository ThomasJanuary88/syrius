let raven_list = [];
let id_dom = 0;

window.onload = function () {	

	//Ajuster le volume des musics de fond
	document.getElementById("audio1").volume=0.2;
	document.getElementById("audio2").pause();

	setTimeout(() => {
		document.getElementById("audio2").volume=0.03;
	document.getElementById("audio2").play();
	}, 4000);

	//Création d'un premier corbeau puis un autre toutes les
	// 4,62 secondes
	create_raven();
	setInterval(create_raven, 4620);

	//Boucle principale qui bouge les corbeaux
	main_tick();

}

function create_raven() {
	//Initalisation des valeurs pour construire un objet corbeau
	let scale = Math.random()* 0.5 + 0.1;
  	let id = "raven"+id_dom;

	//Futur objet corbeau
	let raven = null;

	//Création de l'objet corbeau en fonction de la direction
	let direction = Math.floor(Math.random()* 2 + 1);

	if (direction == 1) {
		type = "raven_right";
		raven = new Raven(scale, type, id);
	}
	else if (direction == 2) {
		type = "raven_left";
		raven = new Raven(scale, type, id);
	}

	//Ajout de l'objet corbeau dans une liste
	raven_list.push(raven);
  	id_dom +=1;
}

function main_tick() {
	for (let i = 0; i < raven_list.length; i++) {
		raven = raven_list[i];
		raven.tick();
		//Supprimer le corbeau s'il sort de l'écran
		if (!raven.immune) {
			if (raven.x < -150 || raven.x > document.body.offsetWidth) {
				raven.delete_node();
				raven_list.splice(i, 1);
				i--;
			}
		}
	}
	window.requestAnimationFrame(main_tick);
}

class Raven {
	constructor(scale, type, id) {
		this.scale = scale;
		this.type = type;
		this.node = document.createElement("div");
		this.node.id = id;
		this.append_node();
		this.sprite = new TiledImage("assets/images/items/"+this.type +".png", 9, 3, 100, true, this.scale, this.node.id);
		this.sprite.changeMinMaxInterval(1, 9);	
		this.immune = true;
		this.initialise_position(); //and speed
		this.disable_immunity();
	}

	append_node() {
		document.body.appendChild(this.node);
	}

	delete_node() {
		this.node.remove();
	}

	initialise_position() {
		if (this.type == "raven_right") {
			this.x = -100;
			this.y = Math.random()* this.scale*100 + 50;
			this.speed = this.scale*5;
		}
		else if (this.type == "raven_left"){
			this.x = document.body.offsetWidth + 100;
			this.y = Math.random()* this.scale*100 + 50
			this.speed = -this.scale*5;
		}
	}
	//Fonction qui sert à ne pas supprimer un corbeau
	//lors de sa création
	disable_immunity() {
		setTimeout(() => {
			this.immune = false;
		}, 2000);
	}
	tick () {
		this.x += this.speed;
		this.node.style.left = this.x + "px";
		this.sprite.tick(this.x, this.y);
	}
}