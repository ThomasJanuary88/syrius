let game_logic = null;
let list_turn = [];
let premier_appel = true;
let fx = null;
let missionLevel;

window.onload = function() {
	let screenGame = document.getElementById("screen_game");

	missionLevel = decodeURI( $_GET( 'missionLevel' ) );

	screenGame.setAttribute("style", "background-image: url('assets/images/backgrounds/dungeon_room"+missionLevel+".jpg');");


	
	fx = document.getElementById('fx');
	setInterval(() => { call_api_state_game() }, 4000);

	//Loadingscreen
	loadingScreen();
}
class Game_logic {
	constructor(player, boss, list_other_player) {
		this.player = player;
		this.boss = boss;
		this.list_other_player = list_other_player;
	}
	tick() {
		for (let i = 0; i < list_turn.length; i++) {
			console.log(list_turn[i]);	
		}
		if (list_turn.length > 0) {
			switch (list_turn[0][0]) {
				case "Player":
					game_logic.player.attack(list_turn[0][1]);
					list_turn.shift();
				break;

				case "other_player0":
					game_logic.list_other_player[0].attack(list_turn[0][1]);
					list_turn.shift();
				break;

				case "other_player1":
					game_logic.list_other_player[1].attack(list_turn[0][1]);
					list_turn.shift();

				break;

				case "other_player2":
					game_logic.list_other_player[2].attack(list_turn[0][1]);
					list_turn.shift();

				break;

				case "boss":
					game_logic.boss.attack(list_turn[0][1]);
					list_turn.shift();

				break;
			}
		}
	}
}

class Player {
	constructor(name, type, hp, hp_max, mp, mp_max, skills) {
		this.node = document.getElementById("player")
    	this.node_image = document.getElementById("player_image");
		this.node_hp = document.getElementById("player_life_bar");
		this.node_mp = document.getElementById("player_mp_bar");
		this.node_portrait = document.getElementById("portrait");
		this.skills = skills;
		this.attack_backgrounds = [];
		this.skill_are_set = false;
		this.alive = true;

		this.name = name;
		this.type = type;
		this.hp = hp;
		this.hp_max = hp_max;
		this.mp = mp;
		this.mp_max = mp;

		this.idle();
		this.create_icon_ability();
		this.create_portrait();
		this.create_life_mp_bar();
	}


	set_life_mp_bar(hp, mp) {
		this.node_hp.value = hp;
		this.node_mp.value = mp;
	}

	create_life_mp_bar() {
		this.node_hp.value = this.hp;
		this.node_hp.max = this.hp_max;

		this.node_mp.value = this.mp;
		this.node_mp.max = this.mp_max;
	}

	create_portrait() {
		this.node_portrait.style.background ="url('assets/images/characters/"+this.type+"/portrait.png'";
		this.node_portrait.style.backgroundRepeat = "no-repeat";
		this.node_portrait.style.backgroundSize = "contain";
	}

	create_icon_ability() {
		for (let i = 0; i < this.skills.length; i++) {
			let ability_name = this.skills[i].name;

			switch(ability_name) {

				case "Normal":
					this.skills[i].node = document.getElementById("spell_"+0);
					this.skills[i].node.style.background = "url('assets/images/characters/"+this.type+"/iconAbility"+0+".png'";
					this.skills[i].node.style.backgroundRepeat = "no-repeat";
					this.skills[i].node.style.backgroundSize = "contain";
					this.skills[i].node.setAttribute("onclick","attack(this)");

					//on stock les backgrounds pour les attaques
					this.attack_backgrounds.push("url('assets/images/characters/"+this.type+"/animAbility"+0+".png'");
				break;
				case "Special1":

					this.skills[i].node = document.getElementById("spell_"+1);
					this.skills[i].node.style.background = "url('assets/images/characters/"+this.type+"/iconAbility"+1+".png'";
					this.skills[i].node.style.backgroundRepeat = "no-repeat";
					this.skills[i].node.style.backgroundSize = "contain";
					this.skills[i].node.setAttribute("onclick","attack(this)");

					//on stock les backgrounds pour les attaques
					this.attack_backgrounds.push("url('assets/images/characters/"+this.type+"/animAbility"+1+".png'");
				break;
				case "Special2":

					this.skills[i].node = document.getElementById("spell_"+2);
					this.skills[i].node.style.background = "url('assets/images/characters/"+this.type+"/iconAbility"+2+".png'";
					this.skills[i].node.style.backgroundRepeat = "no-repeat";
					this.skills[i].node.style.backgroundSize = "contain";
					this.skills[i].node.setAttribute("onclick","attack(this)");

					//on stock les backgrounds pour les attaques
					this.attack_backgrounds.push("url('assets/images/characters/"+this.type+"/animAbility"+2+".png'");
				break;
			}
		}
	}

	idle() {
		this.node_image.style.background = "url('assets/images/characters/"+this.type+"/animHit.png')";
		// positioner le node parent
		this.node.style.position = "absolute";
		this.node.style.right = "1000px";
		this.node.style.top = "440px";
		// adapter l'image (ca a reset)
		this.node_image.style.backgroundSize = "contain";
		this.node_image.style.backgroundRepeat = "no-repeat";
		this.node_image.style.width = "310px";
		this.node_image.style.height = "370px";
		// vérifier le type du joueur et mettre l'animation
		this.node_image.classList.remove('attack_player');
		this.node_image.classList.add('animated');
	}

	hit () {
		if (this.alive) {
			// vérifier le type du joueur et mettre l'animationthis.node_image.style.width = "310px";
			this.node_image.style.height = "370px";
			this.node_image.style.backgroundSize = "contain";
			this.node_image.style.backgroundRepeat = "no-repeat";
			// vérifier le type du joueur et mettre l'animation
			this.node_image.classList.add('attack_player');
			this.node_image.classList.remove('animated');
			setTimeout(() => { this.idle()}, 1000);
		}
	}

	attack(ability) {
		this.current_ability = ability;
		switch(this.current_ability) {
			case "spell_0":
				send_action_attack_server("Normal");
			break;
			case "spell_1":
				send_action_attack_server("Special1");

			break;
			case "spell_2":
				send_action_attack_server("Special2");

			break;
		}
	}

	attack_animation(data) {
		if (data == 'OK') {
			switch(this.current_ability) {
				case "spell_0":
					this.node_image.style.background = this.attack_backgrounds[0];
					fx.style.backgroundImage = "url('assets/images/characters/"+this.type+"/fxAbility0.png')";

					setTimeout(() => {
						fx.style.backgroundImage = "";
					}, 1000);

					play_sound("assets/sounds/"+this.type+"/ability0.mp3", 1.0);
				break;
				case "spell_1":
					this.node_image.style.background = this.attack_backgrounds[1];

					fx.style.backgroundImage = "url('assets/images/characters/"+this.type+"/fxAbility1.png')";

					setTimeout(() => {
						fx.style.backgroundImage = "";
					}, 1000);
					play_sound("assets/sounds/"+this.type+"/ability1.mp3", 1.0);

				break;
				case "spell_2":
					this.node_image.style.background = this.attack_backgrounds[2];

					fx.style.backgroundImage = "url('assets/images/characters/"+this.type+"/fxAbility2.png')";

					setTimeout(() => {
						fx.style.backgroundImage = "";
					}, 1000);
					play_sound("assets/sounds/"+this.type+"/ability2.mp3", 1.0);

				break;
			}
			//changer l'image du boss
			game_logic.boss.hit();
			// positioner le node parent
			this.node.style.position = "absolute";
			this.node.style.right = "1000px";
			this.node.style.top = "440px";
			// adapter l'image (ca a reset)
			this.node_image.style.width = "310px";
			this.node_image.style.height = "370px";
			this.node_image.style.backgroundSize = "contain";
			this.node_image.style.backgroundRepeat = "no-repeat";
			// vérifier le type du joueur et mettre l'animation
			this.node_image.classList.add('attack_player');
			this.node_image.classList.remove('animated');
			setTimeout(() => { this.idle()}, 1000);	
		}
	}

	die() {
		this.alive = false;
		this.node_image.style.background = "url('assets/images/characters/"+this.type+"/dead.png')";
		// adapter l'image (ca a reset)
		this.node_image.style.width = "310px";
		this.node_image.style.height = "370px";
		this.node_image.style.backgroundSize = "contain";
		this.node_image.style.backgroundRepeat = "no-repeat";
	}
}

function attack(node) {
	let node_id = node.id;
	list_turn.splice(1, 0, ["Player", node_id]);

	play_sound('assets/sounds/clic.mp3', 1);

	for (let i = 0; i < game_logic.player.skills.length; i++){
		game_logic.player.skills[i].node.style.pointerEvents = 'none';
		game_logic.player.skills[i].node.style.opacity = 0.5;
	}
	setTimeout(() => {
		for (let i = 0; i < game_logic.player.skills.length; i++){
				if (game_logic.player.skills[i].node.onclick != null) {
					game_logic.player.skills[i].node.style.pointerEvents = 'auto';
					game_logic.player.skills[i].node.style.opacity = 1;
				}
				else {
					game_logic.player.skills[i].node.style.opacity = 0.5;
				}
			}}, 4000);
}

class Other_player {
	constructor(name, type, hp, hp_max, pos) {
		this.name = name;
		this.type = type;
		this.hp = hp;
		this.hp_max = hp_max;
		this.tag = "other_player"+pos;
		this.node = document.getElementById("other_player"+pos);
    	this.node_image = document.getElementById("other_player"+pos+"_image");
		this.node_hp = document.getElementById("other_player"+pos+"_life_bar");
		this.node_name = document.getElementById("other_player"+pos+"_name");
		this.node_name.innerHTML = name;
		this.node_image.classList.add('animated');
		this.distance = 1000 + ((pos + 1) * 130);
		this.animation_attack_name = "attack_other_player"+pos;
		this.node.style.visibility = "visible";
		this.attack_backgrounds = [];
		this.alive = true;
		
		this.node_image.style.background = "url('assets/images/characters/"+this.type+"/animHit.png')";

		for (let i = 0; i < 3; i++) {
			//on stock les backgrounds pour les attaques
			this.attack_backgrounds.push("url('assets/images/characters/"+this.type+"/animAbility"+i+".png'");
		}

		this.idle();
		this.create_life_bar();
	
	}
	die() {
		this.alive = false;
		this.node_image.style.background = "url('assets/images/characters/"+this.type+"/dead.png')";
		this.node_image.style.width = "310px";
		this.node_image.style.height = "370px";
		this.node_image.style.backgroundSize = "contain";
		this.node_image.style.backgroundRepeat = "no-repeat";
	}

	set_life_bar(hp) {
		this.node_hp.value = hp;
	}

	create_life_bar() {
		this.node_hp.value = this.hp;
		this.node_hp.max = this.hp_max;
	}

	attack(ability) {
		switch(ability) {
			case "Normal":
				play_sound("assets/sounds/"+this.type+"/ability0.mp3", 1.0);

				fx.style.backgroundImage = "url('assets/images/characters/"+this.type+"/fxAbility0.png')";

				setTimeout(() => {
					fx.style.backgroundImage = "";
				}, 1000);
				this.node_image.style.background = this.attack_backgrounds[0];

			break;
			case "Special1":
				play_sound("assets/sounds/"+this.type+"/ability1.mp3", 1.0);

				fx.style.backgroundImage = "url('assets/images/characters/"+this.type+"/fxAbility1.png)";

				setTimeout(() => {
					fx.style.backgroundImage = "";
				}, 1000);
				this.node_image.style.background = this.attack_backgrounds[1];

			break;
			case "Special2":
				play_sound("assets/sounds/"+this.type+"/ability2.mp3", 1.0);

				fx.style.backgroundImage = "url('assets/images/characters/"+this.type+"/fxAbility2.png)";

				setTimeout(() => {
					fx.style.backgroundImage = "";
				}, 1000);
				this.node_image.style.background = this.attack_backgrounds[2];
			break;
		}
		//changer l'image du boss
		game_logic.boss.hit();
		this.node_image.style.width = "310px";
		this.node_image.style.height = "370px";
		this.node_image.style.backgroundSize = "contain";
		this.node_image.style.backgroundRepeat = "no-repeat";
		// vérifier le type du joueur et mettre l'animation
		this.node_image.classList.add(this.animation_attack_name);
		this.node_image.classList.remove('animated');
		setTimeout(() => { this.idle()}, 1000);
	}
	hit () {
		if (this.alive) {
			// vérifier le type du joueur et mettre l'animationthis.node_image.style.width = "310px";
			this.node_image.style.height = "370px";
			this.node_image.style.backgroundSize = "contain";
			this.node_image.style.backgroundRepeat = "no-repeat";
			// vérifier le type du joueur et mettre l'animation
			this.node_image.classList.add(this.animation_attack_name);
			this.node_image.classList.remove('animated');
			setTimeout(() => { this.idle()}, 1000);
		}
	}
	idle() {
		switch(this.type) {
		    case "Melee":
				this.node_image.style.background = "url('assets/images/characters/melee/animHit.png')";
		    break;

		    case "Range":
				this.node_image.style.background = "url('assets/images/characters/range/animHit.png')";
		    break;

		    case "Heal":
				this.node_image.style.background = "url('assets/images/characters/heal/animHit.png')";
		    break;

		    case "Magic":
				this.node_image.style.background = "url('assets/images/characters/magic/animHit.png')";
		    break;
		}
		// positioner le node parent
		this.node.style.position = "absolute";
		this.node.style.right = this.distance+"px";
		this.node.style.top = "440px";		
		this.node_image.style.width = "310px";
		this.node_image.style.height = "370px";
		this.node_image.style.backgroundSize = "contain";
		this.node_image.style.backgroundRepeat = "no-repeat";
		// vérifier le type du joueur et mettre l'animation
		this.node_image.classList.remove(this.animation_attack_name);
		this.node_image.classList.add('animated');
	}
		
}

class Boss {
	constructor(type, hp, hp_max) {
		this.node = document.getElementById("boss");
		this.node_image = document.getElementById("boss_image");
		this.node_life = document.getElementById("boss_life_bar");
		this.node_portrait = document.getElementById("portrait_boss");
		this.type = type;
		this.hp = hp;
		this.hp_max = hp_max;
		this.alive = true;

		this.idle();

		this.create_life_bar();
		this.create_portrait();
	}

	die() {
		this.alive = false;
		this.node_image.style.background = "url('assets/images/boss/"+this.type+"/dead.png)";
		this.node_image.style.width = "800px";
		this.node_image.style.height = "520px";

		this.node_image.style.backgroundSize = "contain";
		this.node_image.style.backgroundRepeat = "no-repeat";
	}
	create_portrait() {
		this.node_portrait.style.background ="url('assets/images/boss/"+this.type+"/portrait.png'";
		this.node_portrait.style.backgroundRepeat = "no-repeat";
		this.node_portrait.style.backgroundSize = "contain";
	}

	set_life_bar(hp) {
		this.node_life.value = hp;
	}

	create_life_bar() {
		this.node_life.value = this.hp;
		this.node_life.max = this.hp_max;
	}

	idle() {
		if (this.alive) {
			this.node.style.position = "absolute";
			this.node.style.right = "-20px";
			this.node.style.top = "310px";

			this.node_image.style.background = "url('assets/images/boss/"+this.type+"/animIdle.png')";

			this.node_image.style.width = "800px";
			this.node_image.style.height = "520px";

			this.node_image.style.backgroundSize = "contain";
			this.node_image.style.backgroundRepeat = "no-repeat";
			// vérifier le type du joueur et mettre l'animation
			this.node_image.classList.remove('attack_boss');
			this.node_image.classList.add('animated');
		}
	}
	attack(target_name) {
		if (this.alive) {
			this.node.style.position = "absolute";
			this.node.style.right = "-20px";
			this.node.style.top = "310px";

			this.node_image.style.background = "url('assets/images/boss/"+this.type+"/animAttack.png')";

			this.node_image.style.width = "800px";
			this.node_image.style.height = "520px";
			this.node_image.style.backgroundSize = "contain";
			this.node_image.style.backgroundRepeat = "no-repeat";

			play_sound("assets/sounds/boss/ability0.mp3");

			if (target_name == game_logic.player.name) {
				game_logic.player.hit();
			}
			else if (target_name == 'party') {
				game_logic.player.hit();
				for (let i = 0; i < game_logic.list_other_player.length; i++) {
					game_logic.list_other_player[i].hit();
				}
			}
			else {
				for (let i = 0; i < game_logic.list_other_player.length; i++) {
					if (game_logic.list_other_player[i].name == target_name) {
						game_logic.list_other_player[i].hit();
					}
				}
			}

			this.node_image.classList.remove('animated');
			this.node_image.classList.add('attack_boss');
			setTimeout(() => { this.idle()}, 1000);
		}
	}
	hit () {
		if (this.alive) {
			this.node_image.style.width = "800px";
			this.node_image.style.height = "520px";

			this.node_image.style.background = "url('assets/images/boss/"+this.type+"/animHit.png')";

			this.node_image.style.backgroundSize = "contain";
			this.node_image.style.backgroundRepeat = "no-repeat";


			this.node_image.classList.remove('animated');
			this.node_image.classList.add('attack_boss');
			setTimeout(() => { this.idle()}, 1000);
		}
	}
}

function send_action_attack_server(ability) {
	$.ajax({
		type : "POST",
		url : "ajaxAttack.php",
		data : {
			type : ability
		}
	})
	.done(response => {
		data = JSON.parse(response);
		game_logic.player.attack_animation(data);
	});
}

function call_api_state_game() {
	$.ajax({
			type : "POST",
			url : "ajaxState.php"
		})
		.done(response => {
			data = JSON.parse(response);

			if (typeof data == "object") {
				if (premier_appel) {

				let player = new Player(data.player.name,
					data.player.type,
					data.player.hp,
					data.player.max_hp,
					data.player.mp,
					data.player.max_mp,
					data.player.skills);

				let boss = new Boss(data.game.type,
					data.game.hp,
					data.game.max_hp);

				let list_other_player = [];

				for (let i = 0; i < data.other_players.length; i++) {
					let other_player = new Other_player( data.other_players[i].name,
						data.other_players[i].type,
						data.other_players[i].hp,
						data.other_players[i].max_hp,
						i);
				
					list_other_player.push(other_player);
				}


				game_logic = new Game_logic(player, boss, list_other_player);


				setInterval(() => { game_logic.tick(); }, 1050);
			}
			else {
				//on vérifie si des joueurs se sont deconnetes
				if (data.other_players.length != game_logic.list_other_player.length) {
					for (let i = 0; i < game_logic.list_other_player.length; i++) {
						game_logic.list_other_player[i].node.style.visibility = "hidden";
					}
					game_logic.list_other_player = [];
					for (let i = 0; i < data.other_players.length; i++) {
						new_player = new Other_player(
							data.other_players[i].name,
							data.other_players[i].type,
							data.other_players[i].hp,
							data.other_players[i].max_hp,
							i)
						game_logic.list_other_player.push(new_player);
					}
				}

				//on set les barres de vies ou de mp
				game_logic.boss.set_life_bar(data.game.hp);
				game_logic.player.set_life_mp_bar(data.player.hp, data.player.mp);
				for (let i = 0; i < data.other_players.length; i++) {
					let hp = data.other_players[i].hp;
					game_logic.list_other_player[i].set_life_bar(hp);
					if (hp <= 0) {
						game_logic.list_other_player[i].die();
					}
				}

				// on inscrit les joueurs et le boss qui attaquent dans la liste des tours
				if (data.game.attacked) {
					list_turn.splice(0, 0, ["boss", data.game.last_target]);
				}

				// on vérifie si les autres joueurs ont attaqué
				for (let i = 0; i < data.other_players.length; i++) {
					let other_player = data.other_players[i];
					if (other_player.attacked == "Normal") {

						list_turn.push([game_logic.list_other_player[i].tag, "Normal"]);
					}
					else if (other_player.attacked == "Special1") {

						list_turn.push([game_logic.list_other_player[i].tag, "Special1"]);
					}
					else if (other_player.attacked == "Special2") {

						list_turn.push([game_logic.list_other_player[i].tag, "Special2"]);
					}
				}

				// on verifie les mp du joueur
				let player_mp = data.player.mp;
				console.log('player_mp ' + player_mp);
				for (let i = 0; i < data.player.skills.length; i++) {
					let player_ability_mp = data.player.skills[i].cost;
					if (player_ability_mp - player_mp >= player_mp ) {
						document.getElementById("spell_"+i).onclick = null;
						giveFeedback("Not enought mana");
					}
				}
			}
		}
		else {
			console.log(data);
			if (game_logic != null) {
				list_turn.splice(0, 0, ["boss", game_logic.player.name]);
			}
			if (data == "TOO_MANY_CALLS_BAN") {
				window.location = "index.php";
			}
			else {

				if (data == "GAME_NOT_FOUND_LOST") {
					game_logic.player.die();
				}
				else if (data == "GAME_NOT_FOUND_WIN") {
					list_turn = [];
					game_logic.boss.die();
				}
				setTimeout(() => {
					window.location = "gameMenu.php";
				},4000);
			}
		}
		premier_appel = false;
	});
}

function giveFeedback(message) {
	let node = document.createElement('div');
	node.id = 'feedbackForUser';


	let text = document.createElement('p');
	node.appendChild(text);
}

//https://www.creativejuiz.fr/blog/javascript/recuperer-parametres-get-url-javascript
function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}


function play_sound(path, volume=0.1) {
	let a = new Audio(path);
	a.volume = volume;
	a.play();
}

function goToGameMenu(element) {
	play_sound('assets/sounds/clic.mp3', 1);

	setTimeout(() => {
		element.submit();
	}, 250);
}

function loadingScreen() {
	let nbNodes = document.body.childElementCount+4;
	let soliderAnimated = new SoldierSprite();
	for (let i = 0; i < nbNodes; i++){
		let node = document.body.childNodes[i];
		if (node.style != undefined) {
			node.style.visibility = 'hidden';
		}
		
	}
	let loadingScreen = document.getElementById('loadingScreen');
	loadingScreen.style.visibility = 'visible';

	setTimeout(() => {
		for (let i = 0; i < nbNodes; i++){
			let node = document.body.childNodes[i];
			if (node.style != undefined && node.className != "vertical_menu") {
				node.style.visibility = 'visible';
			}
			
		}
		let loadingScreen = document.getElementById('loadingScreen');
		loadingScreen.style.visibility = 'hidden';

		//Ajuster le volume des musics de fond
		
		
		let audio = document.createElement("AUDIO");
		audio.autoplay = true;
		audio.loop = true;
		audio.src = "assets/musics/game_ongoing/background"+missionLevel+".mp3";
		audio.volume = 0.15;
	},3300);
}

class SoldierSprite {
	constructor() {
		this.node = document.getElementById('loadingAnimation');
		this.x = this.node.offsetLeft;
		this.y = this.node.offsetTop;
		this.sprite = new TiledImage("assets/images/items/loading_screen.png", 30, 1, 10, true, 1.0, this.node.id);
		this.sprite.changeMinMaxInterval(1, 29);
		this.tick();
	}
	tick() {
		setInterval(() => {
			this.sprite.tick(this.x, this.y);
		},40);
	}
}