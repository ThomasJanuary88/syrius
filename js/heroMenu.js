
let player = {
	username : "",
	level : "",
	base_level_exp : "",
	next_level_exp : "",
	exp : "",
	unspent_skills : "",
	welcome_text : "",
	char_vitality : "",
	char_energy : "",
	char_strength : "",
	hp : "",
	mp : "",
	dodge_chance : "",
	dmg_red : "",
	type : ""
}

let premierAppel = null;

window.onload = function() {
	
	//Loadingscreen
	loadingScreen();

	premierAppel = true;
	setInterval(() => { call_api_info_game() }, 3000);

}
function loadingScreen() {
	let nbNodes = document.body.childElementCount+4;
	let soliderAnimated = new SoldierSprite();
	for (let i = 0; i < nbNodes; i++){
		let node = document.body.childNodes[i];
		if (node.style != undefined) {
			node.style.visibility = 'hidden';
		}
		
	}
	let loadingScreen = document.getElementById('loadingScreen');
	loadingScreen.style.visibility = 'visible';

	setTimeout(() => {
		for (let i = 0; i < nbNodes; i++){
			let node = document.body.childNodes[i];
			if (node.style != undefined && node.className != "vertical_menu") {
				node.style.visibility = 'visible';
			}
			
		}
		let loadingScreen = document.getElementById('loadingScreen');
		loadingScreen.style.visibility = 'hidden';

		//Ajuster le volume des musics de fond
		
		createAudio();
	},3300);
}

class SoldierSprite {
	constructor() {
		this.node = document.getElementById('loadingAnimation');
		this.x = this.node.offsetLeft;
		this.y = this.node.offsetTop;
		this.sprite = new TiledImage("assets/images/items/loading_screen.png", 30, 1, 10, true, 1.0, this.node.id);
		this.sprite.changeMinMaxInterval(1, 29);
		this.tick();
	}
	tick() {
		setInterval(() => {
			this.sprite.tick(this.x, this.y);
		},40);
	}
}

function createAudio() {
	let audio = document.createElement("AUDIO");
	audio.autoplay = true;
	audio.loop = true;
	audio.src = "assets/musics/hero_menu/background.mp3";
	audio.volume = 0.15;
}

function call_api_info_game() {
	$.ajax({
			type : "POST",
			url : "ajaxInfo.php",
			data : {
				type : "askServerInfo"
			}
		})
		.done(response => {
			if (premierAppel) {
				data = JSON.parse(response);
				updateHero(data);
				setupHeroNode(response);
				showPlayerInfo();

				premierAppel = false;
			}
			else {
				data = JSON.parse(response);

				updateHero(data);
				updatePlayerInfo();
			}
		});
}

function setupHeroNode() {
	let heroNode = document.getElementById("hero");
	heroNode.style.left = "850px";
	heroNode.style.top = "0px"
	heroNode.classList.add('animated');
}


function updateHero(data) {
	player.username = data.username;
	player.level = data.level;
	player.base_level_exp = data.base_level_exp;
	player.next_level_exp = data.next_level_exp;
	player.exp = data.exp;
	player.unspent_skills = data.unspent_skills;
	player.welcome_text = data.welcome_text;
	player.char_vitality = data.char_vitality;
	player.char_energy = data.char_energy;
	player.char_strength = data.char_strength;
	player.hp = data.hp;
	player.mp = data.mp;
	player.dodge_chance = data.dodge_chance;
	player.dmg_red = data.dmg_red;
	player.type = data.type;
}

function showPlayerInfo() {
	let heroNode = document.getElementById("hero");
	let template = document.querySelector("#template");

	for (key in player) {
		let node1 = document.createElement("p");
		node1.className = 'statName';
		node1.innerHTML = key;

		document.getElementById("heroStats").appendChild(node1);

		let node2 = document.createElement("p");
		node2.className = 'statValue';
		node2.innerHTML = player[key];

		document.getElementById("heroStats").appendChild(node2);

		let br = document.createElement("br");
		document.getElementById("heroStats").appendChild(br);
	}
	
	heroNode.style.backgroundImage ="url('assets/images/characters/"+player.type+"/animSitdown.png'";
}

function updatePlayerInfo() {
	let heroNode = document.getElementById("hero");
	let allStatNameNode = document.getElementsByClassName('statName');
	let allStatValueNode = document.getElementsByClassName('statValue');

	for (let i = 0; i < allStatValueNode.length; i++) {
		let key = allStatNameNode[i].innerHTML;
		allStatValueNode[i].innerHTML = player[key];
	}

	heroNode.style.backgroundImage ="url('assets/images/characters/"+player.type+"/animSitdown.png'";
}

function play_sound(path, volume=0.1) {
	let a = new Audio(path);
	a.volume = volume;
	a.play();
}

function goToGameMenu(element) {
	play_sound('assets/sounds/clic.mp3', 1);

	setTimeout(() => {
		element.submit();
	}, 250);

}