<!DOCTYPE html>
<html>
<head>
	<title>Little Star Wars</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/inGame.css">
	<script type="text/javascript" src="js/TiledImage.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/inGame.js"></script>
	<style>
	* {
		margin:0;
		padding: 0;
	}
	body {		
		background-color: black;
	}

	#screen_game {
		background-repeat: no-repeat;
		background-position: top;
		background-size: cover;
		width: 100%;
		height:85vh;
	}
	#screen_action {
		height: 20vh;
		width:100%;
		padding-top:3%;
	}

	#spells {
		height:50%;
		width:30%;
		display: flex;
		margin:auto;
	}

	#portrait {
		width:180px;
		height:180px;
		display: inline-block;
	}

	#spell_0 {
		padding:0 10px;
		width:300px;
		height:100%;
		z-index: 1;
	}

	#spell_1 {
		padding:0 10px;
		width:300px;
		height:100%;
		z-index: 1;
	}

	#spell_2 {
		padding:0 10px;
		width:300px;
		height:100%;
		z-index: 1;
	}

	#player {
		position:absolute;
	}

	#other_player0 {
		position:absolute;
	}

	#other_player1 {
		position:absolute;
	}

	#other_player2 {
		position:absolute;
	}

	#stats_player {
		padding: 3% 0px 0px 10%;
		width:450px;
		position:absolute;
	}

	#stats_boss {
		padding: 3.65% 0px 0px calc(90% - 450px);
		width:450px;
		position:absolute;
	}

	</style>
</head>
<body >

	<div id="loadingScreen">
		<div id="loadingAnimation"></div>
	</div>
	
	<form action="gameMenu.php" method="post" id="comeBack" onclick="goToGameMenu(this)">
		<button type="submit" id="comeBackButton"></button>
	</form>
	<div id="screen_game">

		<div id="stats_player">
			<div id="portrait"></div>
			<div id="life_mp">
				<progress id="player_life_bar"></progress>
				<progress id="player_mp_bar"></progress>
			</div>
		</div>
		
		<div id="stats_boss">
			<div id="life_boss">
				<progress id="boss_life_bar"></progress>
			</div>
			<div id="portrait_boss"></div>
		</div>

		<div id="boss">
			<div class="entity" id="boss_image"></div>
		</div>

		<div id="player">
			<div id="player_name"></div>
			<div class="entity" id="player_image"></div>
		</div>

		<div id="other_player0">
			<div id="other_player0_name"></div>
			<div class="entity" id="other_player0_image"></div>
			<div class="contain_life_bar"><progress id="other_player0_life_bar"></progress></div>
		</div>

		<div id="other_player1">
			<div id="other_player1_name"></div>
			<div class="entity" id="other_player1_image"></div>
			<div class="contain_life_bar"><progress id="other_player1_life_bar"></progress></div>
		</div>

		<div id="other_player2">
			<div id="other_player2_name"></div>
			<div class="entity" id="other_player2_image"></div>
			<div class="contain_life_bar"><progress id="other_player2_life_bar"></progress></div>
		</div>

		<div id="fx">
		</div>
	</div>

	<div id="screen_action">
		<div id="spells">
			<div id="spell_0"></div>
			<div id="spell_1"></div>
			<div id="spell_2"></div>
		</div>
	</div>


</body>
</html>