<?php
	require_once("action/AjaxInfoAction.php");

	$action = new AjaxInfoAction();
	$action->execute();

	echo json_encode($action->result);