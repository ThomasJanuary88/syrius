<?php
	require_once ('action/GameMenuAction.php');
	$action = new GameMenuAction();

	$action->execute();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Missions menu</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/game_menu.css">
	<script type="text/javascript" src="js/TiledImage.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/gameMenu.js"></script>
	<audio hidden controls autoplay id="audio1">
  		<source src="assets/musics/game_menu/background.mp3" type="audio/mpeg">
	</audio>
	<style>
		input {
			visibility: hidden;
		}

		.room {
			background-image:url("assets/images/items/parchment.png");
			color:black;
			background-repeat: no-repeat;
			height :355px;
			width:251px;
   			text-decoration: none;
		}

		.room p {
			width:100%;
			text-align: center;
			font-family: "MyFont";
			font-size: 20px;
			padding:10px 0;
		}

		.room p:first-child {
			padding-top: 70px;
		}

		.vertical_menu {
			position: absolute;	
			margin-top:140px;
			margin-left:200px;
		    width: 268px;
		    height: 710px;
		    overflow-y: auto;
		}

		#vertical_menu_0 {
    		visibility: hidden;
		}

		#vertical_menu_1 {
    		visibility: hidden;
		}

		#vertical_menu_2 {
    		visibility: hidden;
		}

		#vertical_menu_3 {
    		visibility: hidden;
		}

		#vertical_menu_4 {
    		visibility: hidden;
		}
		
		/* width */
		::-webkit-scrollbar {
		    width: 15px;
		}

		/* Track */
		::-webkit-scrollbar-track {
    		box-shadow: inset 0 0 5px grey; 
		    border-radius: 10px;
		}
		 
		/* Handle */
		::-webkit-scrollbar-thumb {
		    background: #dfbf9f; 
		    border-radius: 10px;
		}

		/* Handle on hover */
		::-webkit-scrollbar-thumb:hover {
		    background: #d2a679; 
		}
	</style>
</head>
<body id="body">
	<div id="loadingScreen">
		<div id="loadingAnimation"></div>
	</div>
	<div>
		<form action="heroMenu.php" method="post" id="brouette" onclick="goToHeroMenu(this)" onmouseover="giveFeedback(this)" onmouseout="lowerOpacity()">
			<!-- <button type="submit"></button> -->
		</form>
	</div>
	<div>
		<a href="?logout=true">Deconnection</a>
	</div>

	<div id="feedbackForUser">
		<p id="textFeedback"></p>
	</div>
</body>
</html>