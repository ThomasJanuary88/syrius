<?php
	require_once("action/CommonAction.php");
	require_once("php/Room.php");

	class GameMenuAction extends CommonAction {

		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			if (isset($_GET["roomId"]) && isset($_GET["missionLevel"])) {
				$data = [];
				$data["key"] = $_SESSION["key"];
				$data["id"] = $_GET["roomId"];
				$key = CommonAction::callAPI("enter", $data);
				if ($key == "GAME_ENTERED") {
					header("location:inGame.php?roomId=".$_GET["roomId"]."&missionLevel=".$_GET["missionLevel"]);
					exit;
				}
			}
		}
	}