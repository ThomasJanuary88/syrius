<?php
	require_once("action/CommonAction.php");

	class AjaxInfoAction extends CommonAction {

		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {

			if (!empty($_POST["type"])) {
				if ($_POST["type"] === "askServerInfo") {
					$key = array("key" => $_SESSION["key"]);
					$this->result = CommonAction::callAPI("user-info", $key);
				}
			}
			else {
				$key = array("key" => $_SESSION["key"]);
				$this->result = CommonAction::callAPI("user-info", $key);
			}
		}
	}