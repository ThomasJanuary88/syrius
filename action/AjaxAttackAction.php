<?php
	require_once("action/CommonAction.php");

	class AjaxAttackAction extends CommonAction {

		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			if (!empty($_POST["type"])) {
				$key = [];
				$key["key"] = $_SESSION["key"];
				$key["skill-name"] = $_POST["type"];
				$this->result = CommonAction::callAPI("action", $key);
			}
		}
	}