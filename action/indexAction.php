<?php
	require_once("action/CommonAction.php");

	class IndexAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if (isset($_POST["nom_compte"]) && isset($_POST["mdp"])) {
				$nom_compte = $_POST["nom_compte"];
				$mdp = $_POST["mdp"];
				$data = [];
				$data["username"] = $nom_compte;
				$data["pwd"] = $mdp;
				$key = CommonAction::callAPI("signin", $data);
				if (strlen($key) == 40 ) {
					$_SESSION["key"] = $key;
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_MEMBER;
					header("location:gameMenu.php");
					exit;
				}
			}
		}
	}